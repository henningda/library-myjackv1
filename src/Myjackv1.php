<?php

namespace Dhe\Myjackv1;

class Myjackv1
{
    public function greet(String $sName)
    {
        return 'Hi ' . $sName . '! How are you doing today?';
    }
}